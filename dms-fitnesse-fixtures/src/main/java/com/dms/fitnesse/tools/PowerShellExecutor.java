package com.dms.fitnesse.tools;

import com.profesorfalken.jpowershell.PowerShell;
import com.profesorfalken.jpowershell.PowerShellNotAvailableException;
import com.profesorfalken.jpowershell.PowerShellResponse;
import org.apache.log4j.Logger;

public class PowerShellExecutor {

    private static final Logger logger = Logger.getLogger(PowerShellExecutor.class);

    /**
     * Executes a command into a Power Shell.
     *
     * @param command command to execute
     * @return text containing the output of the command
     */
    public static String execute(String command) {
        logger.info("Command is: " + command);

        PowerShell powerShell = null;
        String result = null;
        try {
            powerShell = PowerShell.openSession();
            PowerShellResponse response = powerShell.executeCommand(command);

            result = response.getCommandOutput();

        } catch (PowerShellNotAvailableException ex) {
            logger.info("Power Shell is not available", ex);

        } finally {
            if (powerShell != null)
                powerShell.close();
        }
        logger.info("Command result: " + result);
        return result;
    }


}
