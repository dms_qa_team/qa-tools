package com.dms.fitnesse.tools;

import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

public class DBConnection {

    private static final Logger logger = Logger.getLogger(DBConnection.class);

    private static final String MYSQL = "mysql";
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String ORACLE = "oracle";
    private static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";

    private Connection connection;
    private String database;
    private String password;
    private SSHConnection sshc;
    private String username;

    /**
     * Checks query result in the database every given seconds until timeout is reached.
     *
     * @param query   query to run in the database
     * @param timeout the timeout established for the method to keep verifying the query result
     * @param sleep   sleep time established between database verifications
     * @return true if the query returns a non-empty result before timeout expires, false otherwise
     */
    public boolean checkQueryResultTimeoutSleep(String query, long timeout, int sleep) {
        logger.info("Checking query result");

        long timeoutExpired = System.currentTimeMillis() + (timeout * 1000);
        logger.debug(query);

        long startTime = System.currentTimeMillis();

        while (true) {
            long currentTime = System.currentTimeMillis();

            if (currentTime >= timeoutExpired) {
                logger.info("Timeout has expired: " + (timeout + (currentTime - timeoutExpired) / 1000.0));
                return false;

            } else {
                logger.debug("Elapsed time: " + (currentTime - startTime) / 1000.0);
            }

            String result = executeQuery(query);

            if (result == null) {
                logger.debug("The query returned no results");

            } else {
                logger.info("The query returned results");
                return true;
            }

            // Sleep for 'sleep' seconds
            try {
                Thread.sleep(sleep * 1000);

            } catch (InterruptedException e) {
                logger.error("Error while sleeping: " + e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Closes the SSH connection tunnel created by the method openSshTunnelTo().
     */
    public void closeSshTunnel() {
        logger.info("Closing SSH tunnel");

        sshc.disconnect();

        logger.debug("SSH tunnel was closed successfully");
    }

    /**
     * Establishes a connection with the database server.
     *
     * @param type type of database to connect ('mysql' and 'oracle' support available)
     * @param url  URL of the database
     * @param port port to connect to the database
     */
    public void connect(String type, String url, int port) {
        logger.info("Connecting to the database: " + url + ":" + port);

        try {
            if (type.toLowerCase().equals(MYSQL)) {
                Class.forName(MYSQL_DRIVER);
                connection = DriverManager.getConnection("jdbc:mysql://" + url + ":" + port + "/" + database, username, password);

            } else if (type.toLowerCase().equals(ORACLE)) {
                Class.forName(ORACLE_DRIVER);
                connection = DriverManager.getConnection("jdbc:oracle:thin:@" + url + ":" + port + ":" + database, username, password);
            }

        } catch (ClassNotFoundException | SQLException e) {
            logger.error("Error connecting to the database: " + e.getMessage());
            e.printStackTrace();
        }

        if (connection == null)
            logger.warn("The database connection could not be established");
        else
            logger.debug("The connection with the database was established successfully");
    }

    /**
     * Disconnects the connection established with the database server.
     */
    public void disconnect() {
        logger.info("Disconnecting database");

        try {
            connection.close();

            logger.debug("Database was disconnected successfully");

        } catch (SQLException e) {
            logger.error("Error disconnecting database: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Executes a query using a established connection to a database server.
     *
     * @param query query to run in the server
     * @return text containing the output of the query as String
     */
    public String executeQuery(String query) {
        logger.info("Executing query in the database");

        String result = null;
        logger.debug(query);

        try {
            if (connection != null) {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);

                while (resultSet.next()) {
                    result = resultSet.getString(1);
                }

                statement.close();
                resultSet.close();

            } else {
                logger.warn("The query could not be executed, the connection was not established");
                return "";
            }

        } catch (SQLException e) {
            logger.error("Error executing query in the database: " + e.getMessage());
            e.printStackTrace();
            return "";
        }

        logger.debug(result);
        return result;
    }

    /**
     * Executes a query using a established connection to a database server.
     *
     * @param query query to run in the server
     * @return an ArrayList containing the columns extracted from the result of the query
     */
    public ArrayList<String> executeQueryList(String query) {
        logger.info("Executing query in the database");

        ArrayList<String> result = new ArrayList<>();
        logger.debug(query);

        try {
            if (connection != null) {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(query);

                ResultSetMetaData rsmd = resultSet.getMetaData();
                int columnCount = rsmd.getColumnCount();

                while (resultSet.next()) {
                    int i = 1;

                    while (i <= columnCount) {
                        result.add(resultSet.getString(i++));
                    }
                }
            }

        } catch (SQLException e) {
            logger.error("Error executing query in the database: " + e.getMessage());
            e.printStackTrace();
            //return "";
        }

        return result;
    }

    /**
     * Establishes a SSH connection that will be used as a tunnel for a remote database access.
     *
     * @param host       URL of the remote server
     * @param port       port to connect to the remote server
     * @param user       username for the SSH connection
     * @param privateKey private key to use with the username
     */
    public void openSshTunnelTo(String host, int port, String user, String privateKey) {
        logger.info("Opening SSH tunnel");

        sshc = new SSHConnection();

        sshc.setHost(host);
        sshc.setPort(port);
        sshc.setUser(user);
        sshc.setPrivateKey(privateKey);

        sshc.connect();

        logger.debug("The tunnel was opened successfully");
    }

    /**
     * Sets the field 'database' to the specified value.
     *
     * @param database value to set the field 'database'
     */
    public void setDatabase(String database) {
        this.database = database;
    }

    /**
     * Sets the field 'password' to the specified value.
     *
     * @param password value to set the field 'password'
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Forwards a local port to a remote host and port where the database is located.
     *
     * @param lport local port to forward
     * @param rhost remote host for the SSH bridge
     * @param rport remote port for the SSH bridge
     */
    public void setPortForwardingTo(int lport, String rhost, int rport) {
        logger.info("Setting port forwarding");

        sshc.setPortForwardingTo(lport, rhost, rport);

        logger.debug("Port forwarding was set successfully");
    }

    /**
     * Sets the field 'username' to the specified value.
     *
     * @param username value to set the field 'username'
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
