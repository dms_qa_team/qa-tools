package com.dms.fitnesse.tools;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ShellExecutor {

    private static final Logger logger = Logger.getLogger(ShellExecutor.class);

    /**
     * Executes a command into a terminal.
     *
     * @param command command to execute
     * @return text containing the output of the command
     */
    public static String execute(String command) {
        logger.info("Executing command: " + command);

        Process process;
        String result;

        try {
            process = Runtime.getRuntime().exec(command);
            process.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuilder output = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            result = output.toString();

            logger.debug("Command executed successfully: " + result);

        } catch (IOException | InterruptedException e) {
            logger.error("Error executing command: " + e.getMessage());
            e.printStackTrace();
            return "";
        }

        return result;
    }

    /**
     * Executes a shell command into a terminal.
     * Capable to execute piped commands.
     *
     * @param command command to execute
     * @return text containing the output of the command
     */
    public static String executeShell(String command) {
        logger.info("Executing command: " + command);

        String[] cmd = {"/bin/sh", "-c", command};
        Process process;
        String result;

        try {
            process = Runtime.getRuntime().exec(cmd);
            BufferedReader is = new BufferedReader(new InputStreamReader(process.getInputStream()));

            StringBuilder output = new StringBuilder();
            String line;
            while ((line = is.readLine()) != null) {
                output.append(line).append("\n");
            }
            result = output.toString().trim();
            logger.debug("Command executed successfully: " + result);
            process.destroy();

        } catch (IOException e) {
            logger.error("Error executing command: " + e.getMessage());
            return "";
        }
        return result;
    }

    /**
     * Executes a shell command and compares with expected result.
     *
     * @param command  shell command to execute
     * @param expected expected result of shell command
     * @return return true if matches else false
     */
    public static boolean runAndCompare(String command, String expected) {
        String commandResult = ShellExecutor.executeShell(command);
        logger.info("Expected: " + expected + " and Command Result is: " + commandResult);

        return expected.equalsIgnoreCase(commandResult);
    }
}
