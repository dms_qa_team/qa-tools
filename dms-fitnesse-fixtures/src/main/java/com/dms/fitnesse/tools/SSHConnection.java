package com.dms.fitnesse.tools;

import com.jcraft.jsch.*;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

public class SSHConnection {

    private static final Logger logger = Logger.getLogger(SSHConnection.class);

    private static final String DEFAULT_KEY = "~/.ssh/id_rsa";

    private String host;
    private String password;
    private int port;
    private String privateKey;
    private Session session;
    private String user;

    /**
     * Creates a JSch object and open a connection with a remote server using the values set in the class variables.
     */
    public void connect() {
        logger.info("Establishing SSH connection");

        JSch jsch = new JSch();

        try {
            if (privateKey == null)
                privateKey = DEFAULT_KEY;

            // Add key file
            if (password == null)
                jsch.addIdentity(privateKey);

            // Create session
            session = jsch.getSession(user, host, port);

            // Avoid com.jcraft.jsch.JSchException: UnknownHostKey
            session.setConfig("StrictHostKeyChecking", "no");

            // Add password
            if (password != null)
                session.setPassword(password);

            // Connect session
            session.connect();

            logger.debug("SSH connection was established successfully");

        } catch (JSchException e) {
            logger.error("Error establishing SSH connection: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Copies a file from the local server to a remote server.
     * @param localFile local file to be copied
     * @param remoteFile destination to copy the local file
     */
    public void copyFileToServer(String localFile, String remoteFile) {
        logger.info("Copying local file to remote server: " + localFile);

        try {
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");

            // Open sftp channel
            channelSftp.connect();

            // Copy file
            channelSftp.put(localFile, remoteFile);

            // Close sftp channel
            channelSftp.disconnect();

            logger.debug("File copied successfully: " + remoteFile);

        } catch (JSchException | SftpException e) {
            logger.error("Error copying file in remote server: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Disconnects the connection established with the remote server
     */
    public void disconnect() {
        logger.info("Disconnecting from remote server");

        // Disconnect session
        session.disconnect();

        logger.debug("Disconnection was completed successfully");
    }

    /**
     * Executes a command in the remote server using the connection opened by connect().
     * @param command command to execute in the remote server
     * @return the output result of the command as String
     */
    public String execute(String command) {
        logger.info("Executing command: " + command);

        String result;

        try {
            ChannelExec channelExec = (ChannelExec) session.openChannel("exec");
            channelExec.setCommand(command);

            // Open exec channel
            channelExec.connect();

            InputStream stream = channelExec.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

            StringBuilder output = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            result = output.toString();

            // Close exec channel
            channelExec.disconnect();

            logger.debug("Command executed successfully: " + result);

        } catch (JSchException | IOException e) {
            logger.error("Error executing command: " + e.getMessage());
            e.printStackTrace();
            return "";
        }

        return result;
    }

    /**
     * Checks billrun last modification time in a remote file every given seconds until timeout is reached.
     * @param fileName filename to check
     * @param timeout the timeout established for the method to keep verifying the billrun status
     * @param sleep sleep time established between file verifications
     * @return true when timeout expires, false otherwise
     */
    public boolean fileLastModificationTimeout(String fileName, long timeout, int sleep) {
        logger.info("Checking file last modification time");

        while (true) {
            long lastTime = lastModificationTime(fileName);
            logger.debug("Last time measured: " + lastTime);

            if (lastTime == -1) {
                logger.warn("The file could not be found");
                return false;
            }

            if (lastTime >= timeout) {
                logger.info("Timeout has expired: " + lastTime);
                return true;
            }

            // Sleep for 'sleep' seconds
            try {
                Thread.sleep(sleep * 1000);

            } catch (InterruptedException e) {
                logger.error("Error while sleeping: " + e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Check if amount of files found in a remote directory is expected
     * @param file name (or it's part) 
     * @param dir directory where these files are located
     * @param num is an expected instances
     * @return true if found expected number of files, false otherwise
     */
    public boolean checkFilesNumberRemote(String file, String dir, String num) {
        logger.info("Checking number of files is expected in a remote directory");
        String result = execute("ls "+dir+"/* | grep "+file+" | wc -l");
        boolean status = result.replaceAll("\\s+", "").equals(num);
        logger.info("Checking number of files in "+dir+" returns "+status); 
        return status;
    }

    /**
     * Check if amount of files found in a remote directory is expected 
     * @param file name (or it's part) 
     * @param dir directory where these files are located
     * @param num is an expected instances
     * @param postfix is a file name part (second)  
     * @return true if found expected number of files, false otherwise
     */
    public boolean checkFilesNumberRemotePost(String file, String dir, String num, String postfix) {
        logger.info("Checking number of files is expected in a remote directory");
        String result = execute("ls "+dir+"/* | grep "+file+" | grep "+postfix+" | wc -l");
        boolean status = result.replaceAll("\\s+", "").equals(num);
        logger.info("Checking number of files in "+dir+" returns "+status);
        return status;
    }

    /**
     * Check and return the command execution status
     * @param command command to execute
     * @param reverse flag, if true, reverse returned status
     * use it when applicable for certain command
     * @return execution status
     */
    public boolean checkCommandStatus(String command, String reverse) {
        logger.info("Checking and return execution status with reverse flag: "+reverse);
        String result = execute(command);
        boolean status = result.replaceAll("\\s+", "").equals("0");
        if (reverse.equals("true"))
           status  = !status;
        logger.info("Checking status returned is: "+status);
        return status;
    }

    /**
     * Retrieves a file from a remote server into the local server.
     * @param remoteFile remote file to be copied
     * @param localFile destination to copy the remote file
     */
    public void getRemoteFile(String remoteFile, String localFile) {
        logger.info("Getting remote file: " + remoteFile);

        try {
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");

            // Open sftp channel
            channelSftp.connect();

            // Get file
            channelSftp.get(remoteFile, localFile);

            // Close sftp channel
            channelSftp.disconnect();

            logger.debug("File downloaded successfully: " + localFile);

        } catch (JSchException | SftpException e) {
            logger.error("Error getting remote file: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Checks the last modification time of a remote file using the connection opened by connect().
     * @param fileName file to use in the remote server
     * @return last modification time of the remote file (in seconds)
     */
    public long lastModificationTime(String fileName) {
        logger.info("Checking last modification time: " + fileName);

        long timeLapse = -1;

        try {
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");

            // Open sftp channel
            channelSftp.connect();

            Vector list = channelSftp.ls(fileName);

            if (!list.isEmpty()) {
                ChannelSftp.LsEntry lsEntry = (ChannelSftp.LsEntry) list.firstElement();

                Date dateModify = new Date(lsEntry.getAttrs().getMTime() * 1000L);
                Date dateNow = new Date(Calendar.getInstance().getTime().getTime());

                timeLapse = TimeUnit.MILLISECONDS.toSeconds(dateNow.getTime() - dateModify.getTime());

                logger.debug("Time elapsed in seconds: " + timeLapse);
            }

            // Close sftp channel
            channelSftp.disconnect();

        } catch (JSchException | SftpException e) {
            logger.error("Error checking last modification time: " + e.getMessage());
            e.printStackTrace();
        }

        return timeLapse;
    }

    /**
     * Sets the field 'host' to the specified value.
     * @param host value to set the field 'host'
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Sets the field 'password' to the specified value.
     * @param password value to set the field 'password'
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets the field 'port' to the specified value.
     * @param port value to set the field 'port'
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Forwards a local port to a remote host and port.
     * @param lport local port to forward
     * @param rhost remote host for the SSH bridge
     * @param rport remote port for the SSH bridge
     */
    public void setPortForwardingTo(int lport, String rhost, int rport) {
        logger.info("Setting port forwarding");

        try {
            int assignedPort = session.setPortForwardingL(lport, rhost, rport);
            logger.debug("Port forwarded from localhost:" + assignedPort + " to " + rhost + ":" + rport);

        } catch (JSchException e) {
            logger.error("Error setting port forwarding: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Sets the field 'privateKey' to the specified value.
     * @param privateKey value to set the field 'privateKey'
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    /**
     * Sets the field 'user' to the specified value.
     * @param user value to set the field 'user'
     */
    public void setUser(String user) {
        this.user = user;
    }
}
