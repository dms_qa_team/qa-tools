package com.dms.fitnesse.fixtures;

import com.dms.fitnesse.tools.ShellExecutor;
import fitlibrary.DoFixture;

public class ShellFixture extends DoFixture {

    public String runCommand(String command) {
        return ShellExecutor.execute(command);
    }

    public String runShell(String command) {
        return ShellExecutor.executeShell(command);
    }
}
