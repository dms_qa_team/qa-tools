package com.dms.fitnesse.fixtures.examples;

import fitlibrary.DoFixture;

import java.util.Arrays;

public class DoExampleFixture extends DoFixture {

    private String letters;

    public char charAt(int position) {
        return letters.charAt(position);
    }

    public boolean charAtValue(int position, char c) {
        return letters.charAt(position) == c;
    }

    public void fillTimesWith(int count, char c) {
        char[] arr = new char[count];
        Arrays.fill(arr,c);
        letters = new String(arr);
    }

    public void setList(char[] array) {
        letters = new String(array);
    }
}
