package com.dms.fitnesse.fixtures;

import com.dms.fitnesse.tools.SSHConnection;
import fit.Fixture;
import fitlibrary.DoFixture;
import org.apache.log4j.Logger;


public class SSHFixture extends DoFixture {

    private static final Logger logger = Logger.getLogger(SSHFixture.class);

    private static final int SLEEP_TIME = 30;

    private String filename;
    private SSHConnection sshc;

    public SSHFixture() {
        sshc = new SSHConnection();
    }

    public void connect() {
        sshc.connect();
    }

    public void copyFileToServer(String localFile, String remoteFile) {
        sshc.copyFileToServer(localFile, remoteFile);
    }

    public void disconnect() {
        sshc.disconnect();
    }

    public boolean fileLastModificationTimeout(String fileName, long timeout) {
        return fileLastModificationTimeout(fileName, timeout, SLEEP_TIME);
    }

    public boolean fileLastModificationTimeout(String fileName, long timeout, int sleep) {
        return sshc.fileLastModificationTimeout(fileName, timeout, sleep);
    }

    public boolean checkFilesNumberRemote(String file, String dir, String number) {
        return sshc.checkFilesNumberRemote(file, dir, number);
    }

    public boolean checkFilesNumberRemotePost(String file, String dir, String number, String postfix) {
        return sshc.checkFilesNumberRemotePost(file, dir, number, postfix);
    }

    public boolean checkCommandStatus(String command, String reverse) {
        return sshc.checkCommandStatus(command, reverse);
    }

    public String getFilename() {
        return filename;
    }

    public void getRemoteFile(String localFile) {
        sshc.getRemoteFile(getFilename(), localFile);
    }

    public void getRemoteFile(String remoteFile, String localFile) {
        sshc.getRemoteFile(remoteFile, localFile);
    }

    public String runCommand(String command) {
        return sshc.execute(command);
    }

    public boolean runAndCompare(String command, String expected) {
        logger.info("Command  is: " + command);
        String commandResult = sshc.execute(command);
        logger.info("Expected: " + expected + " and Command Result is: " + commandResult);
        commandResult = commandResult.substring(0,commandResult.length()-1);
        logger.info("Expected: " + expected + " and Command Result is: " + commandResult);
        return expected.equalsIgnoreCase(commandResult);

    }
    // csv command compaires with expected
    public boolean runAndCompareCsv(String command, String expected) {
        String result = sshc.execute(command).replace("\u0000", "").trim();
        logger.info("Command Result: "+result);
        logger.info("Expected: " + expected + " and Command Result is: " + result);
        return expected.equalsIgnoreCase(result);

    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setFilenameSymbol(String datastore, String symbol) {
        filename = datastore + Fixture.getSymbol(symbol).toString();
    }

    public void setHost(String host) {
        sshc.setHost(host);
    }

    public void setPassword(String password) {
        sshc.setPassword(password);
    }

    public void setPort(int port) {
        sshc.setPort(port);
    }

    public void setPrivateKey(String privateKey) {
        sshc.setPrivateKey(privateKey);
    }

    public void setUser(String user) {
        sshc.setUser(user);
    }

}
