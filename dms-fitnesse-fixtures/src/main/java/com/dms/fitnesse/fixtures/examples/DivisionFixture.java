package com.dms.fitnesse.fixtures.examples;

import fit.ColumnFixture;

public class DivisionFixture extends ColumnFixture {

    private double denominator;
    private double numerator;

    public double quotient() {
        return numerator/denominator;
    }

    public void setDenominator(double denominator) {
        this.denominator = denominator;
    }

    public void setNumerator(double numerator) {
        this.numerator = numerator;
    }
}
