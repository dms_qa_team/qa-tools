package com.dms.fitnesse.fixtures;

import com.dms.fitnesse.tools.DBConnection;
import fitlibrary.DoFixture;

public class DBFixture extends DoFixture {

    private final static int SLEEP_TIME = 30;

    private DBConnection dbc;

    public DBFixture() {
        dbc = new DBConnection();
    }

    public boolean checkQueryResultTimeoutSleep(String query, long timeout, int sleep) {
        return dbc.checkQueryResultTimeoutSleep(query, timeout, sleep);
    }

    public void closeSshTunnel() {
        dbc.closeSshTunnel();
    }

    public void connectToDatabase(String type, String url, int port) {
        dbc.connect(type, url, port);
    }

    public void disconnect() {
        dbc.disconnect();
    }

    public void openSshTunnelTo(String host, int port, String user, String privateKey) {
        dbc.openSshTunnelTo(host, port, user, privateKey);
    }

    public void setDatabase(String database) {
        dbc.setDatabase(database);
    }

    public void setPassword(String password) {
        dbc.setPassword(password);
    }

    public void setPortForwardingTo(int lport, String rhost, int rport) {
        dbc.setPortForwardingTo(lport, rhost, rport);
    }

    public void setUsername(String username) {
        dbc.setUsername(username);
    }

    public String executeQuery(String query) {
        return dbc.executeQuery(query);
    }
}
