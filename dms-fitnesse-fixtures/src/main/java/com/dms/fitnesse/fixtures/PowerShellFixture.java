package com.dms.fitnesse.fixtures;

import com.dms.fitnesse.tools.PowerShellExecutor;
import fitlibrary.DoFixture;

public class PowerShellFixture extends DoFixture {

    public String runCommand(String command) {
        return PowerShellExecutor.execute(command);
    }
}
