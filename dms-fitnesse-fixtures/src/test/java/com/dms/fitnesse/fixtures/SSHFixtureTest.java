package com.dms.fitnesse.fixtures;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class SSHFixtureTest {
    /* test folder directory to store ssh key file */
    private static final String TEST_FOLDER = "";

    private SSHFixture sshf;

    @BeforeClass
    public void setUp() {
        String host = "192.0.0.222";
        int port = 22;
        String user = "root";
        String password = "centosdms3";
        //String privateKey = TEST_FOLDER + "ssh_key_rsa";

        sshf = new SSHFixture();

        sshf.setHost(host);
        sshf.setPort(port);
        sshf.setUser(user);
        sshf.setPassword(password);
        //sshf.setPrivateKey(privateKey);

        sshf.connect();
    }

    @Test
    public void connectAndRunCmdTest01() {
        SSHFixture fixture = new SSHFixture();
        fixture.setHost("192.0.0.222");
        fixture.setPort(22);
        fixture.setUser("root");
        fixture.setPassword("centosdms3");

        fixture.connect();

        String result = fixture.runCommand("pwd");

        assertNotNull(result);

        fixture.disconnect();
    }

    @Test
    public void connectAndRunCmdTest02() {
        SSHFixture fixture = new SSHFixture();
        fixture.setHost("192.0.0.222");
        fixture.setPort(22);
        fixture.setUser("root");
        fixture.setPassword("centosdms3");

        fixture.connect();

        String result = fixture.runCommand("whoami").trim();

        assertEquals(result, "root");

        fixture.disconnect();
    }

    @AfterClass
    public void tearDown() {
        sshf.disconnect();
    }
}
