package com.dms.fitnesse.fixtures;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class ShellFixtureTest {

    private ShellFixture sf;

    @BeforeClass
    public void setUp() {
        sf = new ShellFixture();
    }

    @Test
    public void runCommandTest01() {
        String result = sf.runCommand("pwd");
        System.out.print(result);
        assertNotNull(result);
    }

    @Test
    public void runShellTest(){
        String result = sf.runShell("ps -ef | grep \"java\"");
        assertNotNull(result);

    }

    @AfterClass
    public void tearDown() {

    }
}
