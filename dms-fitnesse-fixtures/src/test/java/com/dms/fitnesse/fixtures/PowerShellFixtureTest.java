package com.dms.fitnesse.fixtures;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class PowerShellFixtureTest {

    private PowerShellFixture ps;

    @BeforeClass
    public void setUp() {
        ps = new PowerShellFixture();
    }

    @Test(enabled = false) /* enable only if it's running on Windows envs */
    public void runCommandTest01() {
        String result = ps.runCommand("Get-Process");
        System.out.print(result);
        assertNotNull(result);
    }

    @Test(enabled = false) /* enable only if it's running on Windows envs */
    public void runCommandTest02() {
        String result = ps.runCommand("Get-WmiObject Win32_BIOS");
        System.out.print(result);
        assertNotNull(result);
    }

    @Test(enabled = false) /* enable only if it's running on Windows envs */
    public void runCommandTest03() {
        String result = ps.runCommand("gl").trim();
        System.out.print(result);
        assertNotNull(result);
    }


    @AfterClass
    public void tearDown() {

    }
}
