package com.dms.fitnesse.fixtures;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class DBFixtureTest {

    private DBFixture dbf;

    @BeforeClass
    public void setUp() {
        String database = "qa_voxtake";
        String username = "root";
        String password = "root123";

        dbf = new DBFixture();

        dbf.setDatabase(database);
        dbf.setUsername(username);
        dbf.setPassword(password);
    }

    @Test
    public void checkQueryResultTimeoutSleepTest01() {
        String type = "mysql";
        String url = "192.0.0.230";
        int port = 3306;

        String query = "SELECT * FROM qa_voxtake.users where USER_ID='test';";
        long timeout = 900;
        int sleep = 5;

        dbf.connectToDatabase(type, url, port);

        // Use custom sleep time
        assertTrue(dbf.checkQueryResultTimeoutSleep(query, timeout, sleep));

        dbf.disconnect();
   }

    @AfterClass
    public void tearDown() {

    }
}
