package com.dms.fitnesse.examples;

import com.dms.fitnesse.fixtures.examples.DivisionFixture;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DivisionFixtureTest {

    private DivisionFixture df;

    @BeforeClass
    public void setUp() {
        df = new DivisionFixture();
    }

    @Test
    public void quotientTest01() {
        df.setNumerator(6);
        df.setDenominator(2);

        double result = df.quotient();

        assertEquals(result, 3.0, 0.0);
    }

    @AfterClass
    public void tearDown() {

    }
}
