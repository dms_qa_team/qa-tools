package com.dms.fitnesse.examples;

import com.dms.fitnesse.fixtures.examples.MultiplicationFixture;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MultiplicationFixtureTest {

    private MultiplicationFixture mf;

    @BeforeClass
    public void setUp() {
        mf = new MultiplicationFixture();
    }

    @Test
    public void productTest01() {
        mf.setOperand1(2);
        mf.setOperand2(3);

        long result = mf.product();

        assertEquals(result, 6);
    }

    @AfterClass
    public void tearDown() {

    }
}
