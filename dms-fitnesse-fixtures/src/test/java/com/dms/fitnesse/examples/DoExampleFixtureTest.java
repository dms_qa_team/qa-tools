package com.dms.fitnesse.examples;

import com.dms.fitnesse.fixtures.examples.DoExampleFixture;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class DoExampleFixtureTest {

    private DoExampleFixture def;

    @BeforeClass
    public void setUp() {
        def = new DoExampleFixture();
    }

    @Test
    public void charAtTest01() {
        char[] array = {'A','B','C','D'};
        def.setList(array);

        assertEquals(def.charAt(2), 'C');
    }

    @Test
    public void charAtValueTest01() {
        def.fillTimesWith(10, 'x');

        assertTrue(def.charAtValue(4, 'x'));
    }

    @AfterClass
    public void tearDown() {

    }
}
