#!/usr/bin/env bash

pid=`ps -ef | grep 'fitnesse-standalone' | grep -v grep | awk '{print $2}'`
if [ -z $pid ];then
   echo 'No FitNesse instance running'
else
   echo "Stopping $pid"
   kill -9 $pid
fi

path=$(pwd)
cd $path
java -jar fitnesse-standalone.jar -p 8081
