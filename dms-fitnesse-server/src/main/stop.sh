#multiple fitnesse processes may be running, grep the java process with  itnesse-standalone and the correct port
FITNESSE_INSTANCE_PORT=8082
pid=`ps -ef | grep 'fitnesse-standalone' | grep $FITNESSE_INSTANCE_PORT | grep -v grep | awk '{print $2}'`
if [ -z $pid ];then
   echo 'No FitNesse instance running'
else
   echo "Stopping $pid"
   kill -9 $pid
fi
