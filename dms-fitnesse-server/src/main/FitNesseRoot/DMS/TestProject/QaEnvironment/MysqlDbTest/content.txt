!|dbfit.MySqlTest|
!4 Mysql Database Test

!4 MySql Database Query Options from DBFit and DBFixture.

!5 OPTION 1:
!5 A connection is established to the Oracle database.
!5 ''Expects: dbUrl, dbPort, dbUser, dbPassword, dbName''
!|Connect|${dbUrl}:${dbPort}|${dbUser}|${dbPassword}|${dbName}|

!5 Select query from DBFit:
|Query|SELECT CLIENT_DESC AS VALUE FROM client_types WHERE CLIENT_ID=1;|
|VALUE                                                                 |
|Free                                                                  |
!5 OPTION 2:
!5 Select query from DBFixture:
!|com.dms.fitnesse.fixtures.DBFixture                                                      |
|set database|${dbName}                                                                    |
|set username|${dbUser}                                                                    |
|set password|${dbPassword}                                                                |
|connect     |${dbType}    |to         |${dbUrl}        |database        |${dbPort}        |
|Show        |execute query|SELECT CLIENT_DESC AS VALUE FROM client_types WHERE CLIENT_ID=1|
|disconnect                                                                                |



